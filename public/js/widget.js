<script src="https://storage.googleapis.com/mrbot-cdn/webchat-latest.js"></script>
// Or you can replace latest with a specific version
<script>
  WebChat.default.init({
    selector: "#webchat",
    //initPayload: "/get_started",
    customData: {"language": "en"}, // arbitrary custom data. Stay minimal as this will be added to the socket
    socketUrl: "http://34.89.139.156/",
    socketPath: "/socket.io/",
    title: "Platform Bot",
    subtitle: "Let's Chat",
  })
</script>
