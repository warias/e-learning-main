FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
COPY ./server.py /app/server.py 
COPY  ./sql_queries.py /app/sql_queries.py
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt 
ENTRYPOINT [ "python" ]
CMD [ "server.py" ]
