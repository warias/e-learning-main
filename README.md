<h1 align="center">
  <br>
   **e-learning Platform Main Website**
  <br>
</h1>
<p align="center">  
 <a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-blue.svg"></a>
</p>

<p align="center">
  This repository contains the <strong> main website to provide online teaching </strong>.
  
</p>

## Table of contents

  * [Common usage](#common-usage)
  * [Installation](#installation)
     * [DevOps Steps](#node-projects)
     * [Bot Project](#web-projects)
  * [Examples](#examples)
     * [Example 1) Every Single commit creates a new model](#example-1-for-every-7-photos-display-an-ad)
     * [Example 2) Protected Branch protect the bot language](#example-2-for-every-4-paragraphs-of-text-include-2-images)
     * [Example 3) Using Model Server with pre-installed infraestructured](#example-3-in-a-group-of-8-related-links-reserve-positions-5-and-6-for-sponsored-links)
  * [Contributing](#contributing)
  * [License](#license)
  * [Special thanks](#special-thanks)

## Common usage

Let's think we havea knowledge base :  **FAQ** and **Answers**.

```javascript
classes = ([puppies, kittens, penguins], ["puppies", "kittens", "penguins"));
```
## Common usage

| `Class - Label`               | `kitten`               | `penguins`                          | `Prediction Vector` |
|-----------------------|-----------------------|-----------------------------------|------------------------------------------------------------------------------|
| [:dog:, :dog:, :dog:] | [:cat:, :cat:, :cat:] | [:penguin:, :penguin:, :penguin:] | [:dog:, :cat:, :penguin:, :dog:, :cat:, :penguin:, :dog:, :cat:, :penguin:] |



## Contributing

You may contribute in several ways like creating new features, fixing bugs, improving documentation and examples
or translating any document here to your language. [Find more information in CONTRIBUTING.md](CONTRIBUTING.md).

## License

[MIT](LICENSE) - Jota Teles - 2017




